const readline = require("readline");
const rl = readline.Interface({
  input: process.stdin,
  output: process.stdout,
});

function volCube() {
  rl.question("Input length of Cube : ", (s) => {
    const volumeCube = s * s * s;
    console.log(`Volume cube is ${volumeCube}`);
    rl.close();
  });
}
function volBeam() {
  rl.question("Input Length of Beam : ", (l) => {
    rl.question("Input wide of Beam : ", (w) => {
      rl.question("Input high of Beam : ", (h) => {
        const volumeBeam = l * w * h;
        console.log(`Volume beam is ${volumeBeam}`);
        rl.close();
      });
    });
  });
}
console.log("===============================");
console.log("Type 1 for counting Beam volume");
console.log("Type 2 for counting Cube volume");
console.log("===============================");
rl.question("What geometry do you count?    ", (choose) => {
  if (choose == 1) {
    volBeam();
  } else if (choose == 2) {
    volCube();
  } else {
    console.log("Your input undefined");
  }
});

rl.on("close", () => {
  process.exit();
});
