const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
  // Code Here
  let clData = clean(data); //clear null
  for (let i = 0; i < clData.length; i++) {
    for (let j = 0; j < clData.length - 1; j++) {
      if (clData[j] > clData[j + 1]) {
        [clData[j], clData[j + 1]] = [clData[j + 1], clData[j]];
      }
    }
  }
  return clData; // clData array kok mas
}

// Should return array
function sortDecending(data) {
  // Code Here
  let clData = clean(data); //clear null
  for (let i = 0; i < clData.length; i++) {
    for (let j = 0; j < clData.length - 1; j++) {
      if (clData[j] < clData[j + 1]) {
        [clData[j], clData[j + 1]] = [clData[j + 1], clData[j]];
      }
    }
  }

  return clData; // clData array kok mas
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
