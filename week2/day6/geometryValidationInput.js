const readline = require("readline");
const rl = readline.Interface({
  input: process.stdin,
  output: process.stdout,
});
function beam(length, width, height) {
  return length * width * height;
}
function cube(length) {
  return length ** 3;
}
function chooseGeometry() {
  console.log("===============================");
  console.log("Type 1 for counting Beam volume");
  console.log("Type 2 for counting Cube volume");
  console.log("===============================");
  rl.question("What Geometry do you count ?", (choose) => {
    if (choose == 1) {
      inputBeam();
    } else if (choose == 2) {
      inputCube();
    } else {
      console.log(`\nYOU SHOULD INPUT NUMBER 1 OR 2 !\n`);
      chooseGeometry();
    }
  });
}
function inputBeam() {
  rl.question("Length: ", function (length) {
    // length++;
    // console.log(length);
    rl.question("Width: ", (width) => {
      rl.question("Height: ", (height) => {
        if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
          console.log(`\nBeam: ${beam(length, width, height)}`);
          rl.close();
        } else {
          console.log(`Length, Width and Height must be a number\n`);
          inputBeam();
        }
      });
    });
  });
}
function inputCube() {
  rl.question("Length: ", (length) => {
    if (!isNaN(length)) {
      console.log(`\nCube : ${cube(length)}`);
      rl.close();
    } else {
      console.log(`Length must be a number\n`);
      inputCube();
    }
  });
}
chooseGeometry();
rl.on("close", () => {
  process.exit();
});
