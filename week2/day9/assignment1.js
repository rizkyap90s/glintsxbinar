const array = ["tomato", "broccoli", "kale", "cabbage", "apple"];

for (let i = 0; i < array.length; i++) {
  array[i] !== "apple"
    ? console.log(
        `${array[i]} is a healthy food, it's definitely worth to eat.`
      )
    : "";
}
