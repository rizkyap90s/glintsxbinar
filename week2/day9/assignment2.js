const readline = require("readline");
const rl = readline.Interface({
  input: process.stdin,
  output: process.stdout,
});
const people = [
  {
    name: "John",
    status: "Positive",
  },
  {
    name: "Mike",
    status: "Suspect",
  },
  {
    name: "Kiki",
    status: "Negative",
  },
  {
    name: "David",
    status: "Negative",
  },
  {
    name: "Kim",
    status: "Positive",
  },
  {
    name: "Gema",
    status: "Suspect",
  },
];
let list = [];
function input() {
  console.log(`
  Input 1 for Positive data
  Input 2 for Negative data
  Input 3 for Suspect data
  =========================
  `);
  rl.question("Input number : ", (choose) => {
    switch (parseInt(choose)) {
      case 1:
        check("Positive");
        break;
      case 2:
        check("Negative");
        break;
      case 3:
        check("Suspect");
        break;
      default:
        console.log("Wrong input, please input 1, 2 or 3");
        input();
        break;
    }
  });
}
function check(str) {
  people.forEach((person) => {
    person.status === str ? list.push(person.name) : "";
  });
  console.log(`people who ${str} are ${list}\n`);
  rl.close();
}
input();
rl.on("close", () => {
  process.exit();
});
