const allMenu = require("../models/allMenu.json");

class CrudControllers {
  getAllData(req, res) {
    try {
      res.status(200).json({
        data: allMenu,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
  getDataById(req, res) {
    try {
      const menu = allMenu.filter((menu) => menu.id === eval(req.params.id));
      res.status(200).json({
        data: menu,
      });
    } catch (error) {}
  }
  createNewData(req, res) {
    try {
      allMenu.push(req.body);
      res.status(201).json({
        data: req.body,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  updateData(req, res) {
    try {
      allMenu.filter((menu) => {
        if (menu.id === eval(req.params.id)) {
          menu.grup_paket = req.body.grup_paket;
          menu.nama_paket = req.body.nama_paket;
          menu.isi_paket = req.body.isi_paket;
          menu.harga_paket = req.body.harga_paket;
          menu.image_paket = req.body.image_paket;
          menu.minimal_pesan = req.body.minimal_pesan;
          res.status(201).json({
            data: menu,
          });
        }
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  deleteData(req, res) {
    try {
      allMenu.filter((menu) => {
        if (menu.id === eval(req.params.id)) {
          allMenu.splice(0, 1);
          res.status(201).json({
            data: allMenu,
          });
        }
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
}

module.exports = new CrudControllers();
