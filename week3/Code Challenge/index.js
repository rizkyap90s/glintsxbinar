const express = require("express");
const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

const crudRoutes = require("./routes/crudRoutes");
app.use("/", crudRoutes);
app.listen(3000, () => console.log("Server is running"));
