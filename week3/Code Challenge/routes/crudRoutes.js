const express = require("express");
const router = express.Router();

const {
  getAllData,
  createNewData,
  updateData,
  deleteData,
  getDataById,
} = require("../controllers/crudController");

router.get("/", getAllData);
router.get("/:id", getDataById);
router.post("/", createNewData);
router.put("/:id", updateData);
router.delete("/:id", deleteData);

module.exports = router;
