const ThreeDimension = require("./threeDimension");

class Cone extends ThreeDimension {
  constructor(radius, side, height) {
    super("Cone");
    this.radius = radius;
    this.side = side;
    this.height = height;
  }

  static PI = 3.14;

  calculateArea() {
    return Cone.PI * this.radius * (this.radius + this.side);
  }

  calculateVolume() {
    return (1 / 3) * Cone.PI * (this.radius * this.radius) * this.height;
  }
}

module.exports = Cone;
