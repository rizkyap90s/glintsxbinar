const ThreeDimension = require("./threeDimension");

//author : rezki
class Cube extends ThreeDimension {
  constructor(length) {
    super("Cube");
    this.length = length;
  }
  calculateVolume() {
    return this.length ** 3;
  }

  calculateArea() {
    return 6 * this.length ** 2;
  }
}

module.exports = Cube;
