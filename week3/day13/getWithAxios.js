const axios = require("axios");
const url1 = "https://jsonplaceholder.typicode.com/posts";
const url2 = "https://jsonplaceholder.typicode.com/posts/1";
const url3 = "https://jsonplaceholder.typicode.com/posts/1/comments";
const url4 = "https://jsonplaceholder.typicode.com/comments?postId=1";

// get with Promise
const fetchWithPromise = (link) => {
  axios.get(link).then((response) => {
    console.log(response.data);
  });
};

// get with Async await
const fetchWithAsync = async (link) => {
  const response = await axios.get(link);
  console.log(response.data);
};

// fetchWithAsync(url1);
// fetchWithAsync(url2);
// fetchWithAsync(url3);
// fetchWithAsync(url4);

// fetchWithPromise(url1);
// fetchWithPromise(url2);
// fetchWithPromise(url3);
// fetchWithPromise(url4);
