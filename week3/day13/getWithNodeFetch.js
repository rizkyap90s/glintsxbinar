const fetch = require("node-fetch");

const url1 = "https://jsonplaceholder.typicode.com/posts";
const url2 = "https://jsonplaceholder.typicode.com/posts/1";
const url3 = "https://jsonplaceholder.typicode.com/posts/1/comments";
const url4 = "https://jsonplaceholder.typicode.com/comments?postId=1";

// get with Promise
const fetchWithPromise = (link) => {
  fetch(link)
    .then((response) => response.json())
    .then((json) => console.log(json));
};

// get with Async await
const fetchWithAsync = async (link) => {
  const response = await fetch(link);
  const data = await response.json();
  console.log(data);
};

// fetchWithPromise(url1);
// fetchWithPromise(url2);
// fetchWithPromise(url3);
// fetchWithPromise(url4);

// fetchWithAsync(url1);
// fetchWithAsync(url2);
// fetchWithAsync(url3);
// fetchWithAsync(url4);
